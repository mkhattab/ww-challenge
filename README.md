# WorkWell Challenge

The purpose of this document is to outline one potential solution to the WorkWell challenge.

## Challenge Description

Using a service on multiple devices is something common nowadays. Using a laptop, a phone, a tablet and even a smart
watch is widely common. Therefore, synchronising data between the different devices is a key feature of any successful
service.

As for Workwell, we deal with a wide range of clients including banks and insurance companies. Therefore, the security
level of the service is a crucial aspect in order to deploy it for such clients.

Requirements are an architecture for a backend that handles instant sync of a simple data model between
different clients in real time and in a secure way.

The following sections describe a mock service, presented as a product, that achieves the above goals.

## Service: Conference Notes

The proposed service is a note-taking service for conference attendees. Users belonging to an organization are able to
view, create and update notes. Notes are versioned, thus concurrent changes and any historical changes are
preserved. Although, not implemented in the proof-of-concept, adding offline-ready clients should be trivial.


## Architecture

### Overview

A potential datastore that could be used for this service is a document database. Session notes are stored as JSON
documents with a version field. A user making edits to an existing note will result in a new copy of the document, with
an incremented version. Thus, multiple concurrent edits are not lost and preserved. The following diagram describes such
a process:

![note-diagram](https://gitlab.com/mkhattab/ww-challenge/raw/master/note-diagram.png "Note diagram")

A note document with version 0 always represents the latest edit. A duplicate copy of the document is created with the next
highest version number. The following pseudocode describes this process:

```python
def save(db, note):
   latest_version = get_latest_version(db, note.id)

   note.version = latest_version
   db.save(note)

   # Overwrite or create v0 document
   note.version = 0
   db.save(note)
```

For document databases such as the proposed database stated below, the above design allows the latest version of the
document to be fetched efficiently, along with all the subsequent version history, **if** the version field is indexed.


### Components

The implementation of the above design will use a serverless-architecture, hosted on AWS. Considerations of the pros and cons are
discussed in the following section. The following list the key components of such an implementation:

* DynamoDB: backend datastore.
* API Gateway: HTTP(S) service endpoint.
* Lambda: serverless function handling requests from API Gateway.
* Cognito: user registration and authentication.

The Lambda function components:

* GraphQL frontend: primary API for querying and updating notes.
* Go: language the Lambda function is written in.

#### Datastore

The service is using DynamoDB as a document store. DynamoDB supports document retrieval using either a `primary key` or
`partition key` **and** `range key`. Secondary indexes can be created, but like many databases there can be significant
overhead. In the case of DynamoDB, selecting all or a subset of fields to be projected in the secondary index have
trade-offs in resource consumption and costs.

The implementation of this service uses a `partition key` and `range key`, named `ID` and `Version` respectively. They
are defined as follows:

* `ID`: the parent entity ID. In terms of one-to-many, this is the parent document. See example below.
* `Version`: a string containing the following concatenated values:
  * version
  * created timestamp
  * entity ID

There are two document types stored in the same DynamoDB table: `Session` and `Note`. In relational terms, there is a
one-to-many relationship between `Session` and `Note`. Thus, a `Note` can belong to only one `Session`, while a
`Session` can have zero or many `Note`s.

An example of an `ID` and `Version` values of a `Note` document:

```json
{
  "ID": "sessionUUID",
  "Version": "v0_<unix-timesamp>_noteUUID"
}
```

To fetch all `Note`s belonging to a `Session`, the service uses `ID` as the `parition key`, filters the `range key` for
prefixes starting with `v0`. Thus, all the current `Note` versions are returned.

To fetch a single `Note` by it's ID. Like the previous operation, the `partition key` is the session ID. However, the
service will query for a range in values from `v0_<unix-min-timestamp>_noteUUID` to
`v0_<unix-max-timestamp>_noteUUID`. Similarly, fetching all the versions of a `Note` uses the following range:
`v1_<unix-min-timestamp>_noteUUID` to `v<max-int>_<unix-max-timestamp>_noteUUID`.

Using the above approach, avoids having to create a secondary index for versions or created timestamp, which results in
lower overhead and reduced costs.

#### Request workflow

In order to access this service, it requires authentication. Authentication is managed by Cognito User Pools, which is
an AWS service. This service passes `JWT` tokens to the web service, which is accessible via API Gateway and Lambda.

Sign-in flow (for web):

1. User is redirected or visits signin page (hosted by Cognito)
2. After successfully, authenticating, page redirects to the preconfigred callback URL with an `authorization code`.
3. The service exchanges this `authorzation code` for a `JWT` access token.
4. Then the service, it parses and verifies the `JWT` access token.
5. Using the access token, the service fetches user metadata, such as username and the `orgID` (explained below).
6. The service sets a cookie using the access token.

Cognito User Pools manages user directories and can support different clients (e.g. iOS and Android) and authorization
flows. Furthermore, federated identity providers can be used, such as social signin, i.e. Google, Facebook login.

Custom metadata, such as `orgID`, can be associated with a user to implement permissions. In this case, `orgID` is used
to limit the result set returned to the user.

Cognito can also manage user registration, i.e. sign up, verification, MFA authentication, etc. For simplicity, the
proof-of-concept disables these features.


### Design considerations

Using a serverless architecture with the above components, especially hosted on AWS, has the following advantages:

* With DynamoDB as the backend datastore, it provides on an *on-demand* API that scales easily with the number of
  requests to the service and complements Lambda functions well.
* Go lang is the ideal choice for Lambda, and serverless in general, because of its fast startup times, resulting in
  lower latencies on cold starts. Also, using Go versus Python, Java or Javascript, typically results in lower memory
  usage and faster execution times, which are both cost factors that could result in lower hosting costs.
* Using GraphQL as the primary API allows service clients, like the web frontend and in the future mobile applications,
  to request only the data it needs, in typically one query, resulting in more efficient payloads. Thus, reducing the
  need for making subsequent requests to fetch related data, which is the case with REST APIs. As such, this can
  contribute to a lower Lambda costs.
* Using Cognito user pools results in a simpler application and quicker time-to-market by not having to reinvent the
  wheel with respect to handling the user signup, login, authentication and authorization workflows.

However, the proposed design is not without disadvantages, namely the following:

* Vendor lock-in with AWS. This can be mitigated by decoupling business logic as much as possible from AWS APIs. For
  instance, a generalized datastore interface (or ORM) could be used for accessing the datastore like the pseudo code in
  the overview above.
* Reduced observability is an unforunate side effect of using managed AWS components such as AWS Gateway and
  Lambda. When things inevitably go wrong, it isn't obivous where and how to fix issues. Downtime can be mitigated by
  designing infrastructure to support deployment across multiple regions.

### Project Structure

The source code for the project lives in the `api/` directory.

```bash
api/
├── api                            # API entry points for running in Lambda or standalone
│   ├── api.go
│   └── api_test.go
├── auth                           # Cognito Authenthication service
│   └── auth.go
├── cmd                            # Commands for running in Lambda or standalone
│   ├── graphql-lambda
│   └── graphql-server
├── dist                           # Contains binary that is packaged and deployed to AWS Lambda function
│   └── graphql-lambda
├── generated                      # Generated GraphQL service stubs
│   └── generated.go
├── models                         # GraphQL models
│   ├── models.go
│   ├── models_generated.go
│   └── models_test.go
├── services                       # Service implementations for GraphQL service and database service
│   ├── database.go
│   ├── dynamodb.go
│   ├── dynamodb_test.go
│   └── resolver.go
├── Makefile
├── deploy.sh
├── go.mod
├── go.sum
├── post-deploy-run-once.sh
├── schema.graphql                 # GraphQL schema
└── template.yaml                  # Cloudformation template, used to automate provisioning and updating of AWS resources
```

### Install

Prerequisites for deploying this service:

1. An AWS account
2. `aws-cli`
3. `aws-sam-cli`
4. Go 1.12+

Assuming `aws-cli` is configured. Create a `app.env` file containing the following environment variables:

```bash
export AWS_REGION=us-west-2
export COGNITO_DOMAIN_NAME=mydomainname # is used in this URL: https://mydomainname.auth.us-west-2.amazoncognito.com
```

Next step is to run `make deploy`, which will build and run the Cloudformation template. After deployment is complete,
run the post deployment script `./post-deploy-run-once.sh`.

To access the service, fetch the service URL:

```bash
aws cloudformation describe-stacks --stack-name conference-notes-api --query 'Stacks[0].Outputs[1].OutputValue' --output text
```

However, the service requires authentication. Therefore, a user account needs to be created first. The following
commands will create and associate the user with an arbitrary orgID.

```bash
COGNITO_USER_POOL_ID=$(aws cloudformation describe-stack-resources \
                             --stack-name conference-notes-api \
                             --query "StackResources[?ResourceType=='AWS::Cognito::UserPool'][PhysicalResourceId]" \
                             --output text)

aws cognito-idp admin-create-user \
    --user-pool-id $COGNITO_USER_POOL_ID \
    --username yourusername \
    --temporary-password temp-password \
    --user-attributes Name=email,Value=youremail@email.com \
                      Name=name,Value="Your Name" \
                      Name=custom:orgID,Value="someOrgID123"
```

On initial sign-in, Cognito will redirect to password change form,
afterwards the access to the service will be granted.

### GraphQL playground

The URL in the previous section points to a GraphQL playground for
making queries against the service. The following queries demonstrate
how to fetch and create sessions and notes.

Get all sessions:

```graphql

query {
   sessions {
      id
      title
      conference
   }
}
```

Get all sessions, with linked notes:

```graphql

query {
   sessions {
      id
      title
      conference
      notes {
        id
        title
        content
      }
   }
}
```

Create a new session:

```graphql

mutation createSession {
  saveSession(input:{title:"My Session", conference:PYCON}) {
    id
  }
}
```

Create a note in a session:

```graphql
mutation createNote {
  saveNote(input:{sessionID:"sessionID", title:"My Note", content:"Note content"}) {
    id
  }
}
```

Updating an existing note:

```graphql
mutation updateNote {
  saveNote(input:{id:"noteID", sessionID:"sessionID", title:"My Note", content:"Note content"}) {
    id
  }
}
```

Getting all versions of a note:

```graphql
query noteVersions {
  noteVersions(id:"noteID", sessionID:"sessionID") {
    id
    version
    title
  }
}
```
