package api

import (
	"context"
	"fmt"
	"log"
	"net/http"

	"github.com/99designs/gqlgen/graphql"
	"github.com/99designs/gqlgen/handler"
	"github.com/akrylysov/algnhsa"
	"github.com/mkhattab/ww-challenge/auth"
	"github.com/mkhattab/ww-challenge/generated"
	"github.com/mkhattab/ww-challenge/services"
)

func StartServer(db services.DatabaseService, addr string, userInfo *auth.UserInfo) {
	http.Handle("/", handler.Playground("ConferenceNotes", "/query"))
	http.Handle("/query", handler.GraphQL(generated.NewExecutableSchema(services.NewResolver(db)),
		handler.ResolverMiddleware(func(ctx context.Context, next graphql.Resolver) (res interface{}, err error) {
			ctx = context.WithValue(ctx, auth.UserInfo{}, userInfo)
			return next(ctx)
		}),
	))

	log.Fatal(http.ListenAndServe(addr, nil))
}

func ValidateAccessToken(authClient *auth.CognitoAuthClient, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		t, err := r.Cookie("access_token")
		if err != nil {
			log.Println("access_token cookie error:", err)
			http.Redirect(w, r, fmt.Sprintf("%s&redirect_uri=https://%s/%s", authClient.HostedSignIn, r.Host, authClient.RedirectURI), http.StatusTemporaryRedirect)
			return
		}

		_, err = authClient.ParseAndVerifyJWT(t.Value)
		if err != nil {
			log.Println("invalid access_token cookie:", err)
			http.Error(w, err.Error(), http.StatusForbidden)
			return
		}

		userInfo, err := authClient.GetUserInfo(t.Value)
		if err != nil {
			log.Println("missing required org ID:", err)
			http.Error(w, err.Error(), http.StatusForbidden)
			return
		}

		ctx := context.WithValue(context.Background(), auth.UserInfo{}, userInfo)
		r = r.WithContext(ctx)
		next.ServeHTTP(w, r)
	})
}

func StartLambda(db services.DatabaseService, authClient *auth.CognitoAuthClient) {
	http.Handle("/", ValidateAccessToken(authClient,
		handler.Playground("ConferenceNotes", "/Prod/query")),
	)
	http.Handle("/query", ValidateAccessToken(authClient,
		handler.GraphQL(generated.NewExecutableSchema(services.NewResolver(db))),
	))
	http.Handle("/callback", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		redirectURI := fmt.Sprintf("https://%s/%s", r.Host, authClient.RedirectURI)
		tokens, err := authClient.GetTokens(r.URL.Query().Get("code"), redirectURI, []string{})
		if err != nil {
			log.Println("Failed to get tokens:", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if tokens.Error != "" {
			log.Println("Tokens error:", tokens.Error)
			http.Error(w, tokens.Error, http.StatusInternalServerError)
			return
		}

		_, err = authClient.ParseAndVerifyJWT(tokens.AccessToken)
		if err != nil {
			log.Println("Invalid access token:", err)
			http.Error(w, err.Error(), http.StatusUnauthorized)
			return
		}

		http.SetCookie(w, &http.Cookie{
			Name:     "access_token",
			Value:    tokens.AccessToken,
			Domain:   r.Host,
			Path:     "/",
			HttpOnly: true,
		})
		http.Redirect(w, r, fmt.Sprintf("https://%s/Prod", r.Host), http.StatusMovedPermanently)

	}))
	algnhsa.ListenAndServe(http.DefaultServeMux, nil)
}
