package api

import (
	"context"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/99designs/gqlgen/client"
	"github.com/99designs/gqlgen/graphql"
	"github.com/99designs/gqlgen/handler"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/mkhattab/ww-challenge/auth"
	"github.com/mkhattab/ww-challenge/generated"
	"github.com/mkhattab/ww-challenge/models"
	"github.com/mkhattab/ww-challenge/services"
)

const tableName string = "ConferenceNotesTestAPITable"

func setup(t *testing.T, client dynamodbiface.DynamoDBAPI) {
	tableName := aws.String(tableName)
	_, err := client.CreateTable(&dynamodb.CreateTableInput{
		TableName: tableName,
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("ID"),
				KeyType:       aws.String("HASH"),
			},
			{
				AttributeName: aws.String("Version"),
				KeyType:       aws.String("RANGE"),
			},
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("ID"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("Version"),
				AttributeType: aws.String("S"),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(5),
			WriteCapacityUnits: aws.Int64(5),
		},
	})
	if err != nil {
		t.Fatalf("Failed to create test table: got %v", err)
	}

	_, err = client.BatchWriteItem(&dynamodb.BatchWriteItemInput{
		RequestItems: map[string][]*dynamodb.WriteRequest{
			*tableName: {
				// Sessions
				{
					PutRequest: &dynamodb.PutRequest{
						Item: map[string]*dynamodb.AttributeValue{
							"ID":         {S: aws.String("uniqOrgID123")},
							"Version":    {S: aws.String("v0_1000000000_uniqSessID123")},
							"Title":      {S: aws.String("Session title")},
							"Conference": {S: aws.String(string(models.ConferenceGophercon))},
						},
					},
				},
				{
					PutRequest: &dynamodb.PutRequest{
						Item: map[string]*dynamodb.AttributeValue{
							"ID":         {S: aws.String("uniqOrgID123")},
							"Version":    {S: aws.String("v1_1000000000_uniqSessID123")},
							"Title":      {S: aws.String("Session title")},
							"Conference": {S: aws.String(string(models.ConferenceGophercon))},
						},
					},
				},
				{
					PutRequest: &dynamodb.PutRequest{
						Item: map[string]*dynamodb.AttributeValue{
							"ID":         {S: aws.String("uniqOrgID123")},
							"Version":    {S: aws.String("v0_1100000000_uniqSessID456")},
							"Title":      {S: aws.String("Session title 2")},
							"Conference": {S: aws.String(string(models.ConferencePycon))},
						},
					},
				},
				{
					PutRequest: &dynamodb.PutRequest{
						Item: map[string]*dynamodb.AttributeValue{
							"ID":         {S: aws.String("uniqOrgID456")},
							"Version":    {S: aws.String("v0_1100000000_uniqSessID789")},
							"Title":      {S: aws.String("Session title 2 (other org)")},
							"Conference": {S: aws.String(string(models.ConferenceGophercon))},
						},
					},
				},

				// Notes
				{
					PutRequest: &dynamodb.PutRequest{
						Item: map[string]*dynamodb.AttributeValue{
							"ID":      {S: aws.String("uniqSessID123")},
							"Version": {S: aws.String("v0_1000000000_uniqNoteID123")},
							"Title":   {S: aws.String("Topic 1")},
							"Content": {S: aws.String("Content 1")},
						},
					},
				},
				{
					PutRequest: &dynamodb.PutRequest{
						Item: map[string]*dynamodb.AttributeValue{
							"ID":      {S: aws.String("uniqSessID123")},
							"Version": {S: aws.String("v1_1000000000_uniqNoteID123")},
							"Title":   {S: aws.String("Topic 1 on Session title")},
							"Content": {S: aws.String("Content 1")},
						},
					},
				},
				{
					PutRequest: &dynamodb.PutRequest{
						Item: map[string]*dynamodb.AttributeValue{
							"ID":      {S: aws.String("uniqSessID123")},
							"Version": {S: aws.String("v0_1100000000_uniqNoteID456")},
							"Title":   {S: aws.String("Topic 2 on Session title 2 ")},
							"Content": {S: aws.String("Content 2")},
						},
					},
				},
				{
					PutRequest: &dynamodb.PutRequest{
						Item: map[string]*dynamodb.AttributeValue{
							"ID":      {S: aws.String("uniqSessID789")},
							"Version": {S: aws.String("v0_1100000000_uniqNoteID789")},
							"Title":   {S: aws.String("Topic 2 on Session title 2 (other org)")},
							"Content": {S: aws.String("Content 2")},
						},
					},
				},
			},
		},
	})
	if err != nil {
		t.Fatalf("Failed to create test items: got %v", err)
	}
}

func teardown(t *testing.T, client dynamodbiface.DynamoDBAPI) {
	_, err := client.DeleteTable(&dynamodb.DeleteTableInput{TableName: aws.String(tableName)})
	if err != nil {
		t.Fatalf("Failed to delete table: got %v", err)
	}
}

func TestAPI(t *testing.T) {
	endpoint := os.Getenv("DYNAMODB_ENDPOINT")
	if endpoint == "" {
		t.Fatal("DYNAMODB_ENDPOINT not set")
	}

	cfg := aws.Config{
		Region:   aws.String("us-west-2"),
		Endpoint: aws.String(endpoint),
	}
	sess := session.Must(session.NewSession())
	dynamoclient := dynamodb.New(sess, &cfg)

	db := services.NewDynamoDB(dynamoclient, tableName)
	db.WithOption("useConsistentReads", true)

	srv := httptest.NewServer(handler.GraphQL(generated.NewExecutableSchema(services.NewResolver(db)),
		handler.ResolverMiddleware(func(ctx context.Context, next graphql.Resolver) (res interface{}, err error) {
			ctx = context.WithValue(ctx, auth.UserInfo{}, &auth.UserInfo{Username: "defaultUser", OrgID: "uniqOrgID123"})
			return next(ctx)
		}),
	))
	c := client.New(srv.URL)

	t.Run("get sessions", func(t *testing.T) {
		setup(t, dynamoclient)
		defer teardown(t, dynamoclient)

		var resp struct {
			Sessions []struct {
				ID    string
				Title string
			}
		}

		c.MustPost(`query { sessions { id title } }`, &resp)
		if len(resp.Sessions) != 2 {
			t.Errorf("count got %v, expected 2", len(resp.Sessions))
		} else {
			s1 := resp.Sessions[0]
			s2 := resp.Sessions[1]

			if s1.ID != "uniqSessID123" {
				t.Errorf("first ID got %v, expected uniqSessID123", s1.ID)
			}

			if s2.ID != "uniqSessID456" {
				t.Errorf("first ID got %v, expected uniqSessID456", s1.ID)
			}
		}
	})

	t.Run("save note", func(t *testing.T) {
		setup(t, dynamoclient)
		defer teardown(t, dynamoclient)

		var resp struct {
			SaveNote struct {
				ID    string
				Title string
			}
		}

		err := c.Post(`mutation { saveNote(input:{sessionID:"uniqSessID123", title:"Foo", content:"Bar"}) { id title } }`, &resp)
		if err != nil {
			t.Errorf("failed to save: got %v", err)
		}

		var notesResp struct {
			Notes []struct {
				ID    string
				Title string
			}
		}
		c.MustPost(`query { notes(sessionID:"uniqSessID123") { id title } }`, &notesResp)

		if len(notesResp.Notes) != 3 {
			t.Errorf("Count got %v, expected 3", len(notesResp.Notes))
		} else {
			n := notesResp.Notes[2]
			if n.ID == "" {
				t.Errorf("ID empty")
			}

			if n.Title != "Foo" {
				t.Errorf("Title got %v, expected Foo", n.Title)
			}

		}
	})
}
