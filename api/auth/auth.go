package auth

import (
	"bytes"
	"crypto/rsa"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/cognitoidentityprovider"
	"github.com/aws/aws-sdk-go/service/cognitoidentityprovider/cognitoidentityprovideriface"
	"github.com/dgrijalva/jwt-go"
	"github.com/lestrrat-go/jwx/jwk"
	"github.com/patrickmn/go-cache"
)

type UserInfo struct {
	Username string
	OrgID    string
}

type CognitoToken struct {
	IDToken      string `json:"id_token"`
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
	ExpiresIn    int    `json:"expires_in"`
	TokenType    string `json:"token_type"`
	Error        string `json:"error"`
}

type CognitoAuthClient struct {
	Domain                   string
	AuthorizeEndpoint        string
	HostedSignIn             string
	RedirectURI              string
	ClientID                 string
	ClientSecret             string
	Base64BasicAuthorization string
	PoolID                   string
	Region                   string
	WellKnownJWKs            *jwk.Set
	CognitoClient            cognitoidentityprovideriface.CognitoIdentityProviderAPI
	UserCache                *cache.Cache
}

func (c *CognitoAuthClient) Init() error {
	set, err := jwk.FetchHTTP(fmt.Sprintf("https://cognito-idp.%s.amazonaws.com/%s/.well-known/jwks.json", c.Region, c.PoolID))
	if err != nil {
		return err
	}

	c.WellKnownJWKs = set
	c.AuthorizeEndpoint = fmt.Sprintf("%s/oauth2/token", c.Domain)
	c.HostedSignIn = fmt.Sprintf("%s/login?response_type=code&client_id=%s", c.Domain, c.ClientID)

	if c.ClientSecret != "" {
		var buffer bytes.Buffer
		buffer.WriteString(c.ClientID)
		buffer.WriteString(":")
		buffer.WriteString(c.ClientSecret)
		base64AuthStr := base64.StdEncoding.EncodeToString(buffer.Bytes())
		buffer.Reset()

		buffer.WriteString("Basic ")
		buffer.WriteString(base64AuthStr)
		c.Base64BasicAuthorization = buffer.String()
		buffer.Reset()
	}

	return nil
}

func (c *CognitoAuthClient) GetTokens(code, redirectURI string, scope []string) (CognitoToken, error) {
	var token CognitoToken

	hc := http.Client{}
	// set the url-encoded payload
	form := url.Values{}
	form.Set("grant_type", "authorization_code")
	form.Set("client_id", c.ClientID)
	form.Set("code", code)
	form.Set("redirect_uri", redirectURI)
	if len(scope) > 0 {
		form.Set("scope", strings.Join(scope, " "))
	}
	// request
	req, err := http.NewRequest("POST", c.AuthorizeEndpoint, strings.NewReader(form.Encode()))
	if err == nil {
		req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
		// This should be a string like: Basic XXXXXXXXXX
		log.Println("Authorization header:", c.Base64BasicAuthorization)
		log.Println("Form values:", form)
		req.Header.Add("Authorization", c.Base64BasicAuthorization)

		resp, err := hc.Do(req)
		if err != nil {
			log.Println("Could not make request to Cognito TOKEN endpoint")
			return token, err
		}
		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Println("Could not read response body from Cognito TOKEN endpoint")
			return token, err
		}

		err = json.Unmarshal(body, &token)
		if err != nil {
			log.Println("Could not unmarshal token response from Cognito TOKEN endpoint")
		}
	} else {
		log.Println("Error making HTTP request", err)
	}
	return token, err
}

func (c *CognitoAuthClient) ParseAndVerifyJWT(t string) (*jwt.Token, error) {
	token, err := jwt.Parse(t, func(token *jwt.Token) (interface{}, error) {
		keys := c.WellKnownJWKs.LookupKeyID(token.Header["kid"].(string))
		if len(keys) == 0 {
			log.Println("Failed to look up JWKs")
			return nil, errors.New("could not find matching `kid` in well known tokens")
		}

		key, err := keys[0].Materialize()
		if err != nil {
			log.Printf("Failed to create public key: %s", err)
			return nil, err
		}
		rsaPublicKey := key.(*rsa.PublicKey)
		return rsaPublicKey, nil
	})

	if err == nil && token.Valid {
		if claims, ok := token.Claims.(jwt.MapClaims); ok {
			err = claims.Valid()
			if err == nil {
				if claims.VerifyAudience(c.ClientID, false) {
					return token, nil
				}
				err = errors.New("token audience does not match client id")
				log.Println("Invalid audience for id token")
			} else {
				log.Println("Invalid claims for id token")
				log.Println(err)
			}
		}
	} else {
		log.Println("Invalid token:", err)
	}

	return nil, err
}

func (c *CognitoAuthClient) GetUserInfo(t string) (*UserInfo, error) {
	var u *UserInfo
	if x, found := c.UserCache.Get(t); found {
		u = x.(*UserInfo)
		c.UserCache.Set(t, u, cache.DefaultExpiration)
		return u, nil
	}

	output, err := c.CognitoClient.GetUser(&cognitoidentityprovider.GetUserInput{AccessToken: aws.String(t)})
	if err != nil {
		return nil, err
	}

	var orgID string
	fieldName := fmt.Sprintf("custom:orgID")
	for _, f := range output.UserAttributes {
		if *f.Name == fieldName {
			orgID = *f.Value
		}
	}

	if orgID == "" {
		return nil, errors.New("required custom orgID attribute not set")
	}

	u = &UserInfo{
		Username: *output.Username,
		OrgID:    orgID,
	}
	c.UserCache.Set(t, u, cache.DefaultExpiration)
	return u, nil
}
