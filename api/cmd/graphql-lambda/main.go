package main

import (
	"log"
	"os"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cognitoidentityprovider"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/mkhattab/ww-challenge/api"
	"github.com/mkhattab/ww-challenge/auth"
	"github.com/mkhattab/ww-challenge/services"
	"github.com/patrickmn/go-cache"
)

func main() {
	table := os.Getenv("DYNAMODB_TABLE")
	if table == "" {
		log.Fatal("DYNAMODB_TABLE environment variable required")
	}

	domain := os.Getenv("COGNITO_DOMAIN")
	if domain == "" {
		log.Fatal("COGNITO_DOMAIN environment variable required")
	}

	cognitoPoolID := os.Getenv("COGNITO_POOL_ID")
	if cognitoPoolID == "" {
		log.Fatal("COGNITO_POOL_ID environment variable required")
	}

	cognitoClientID := os.Getenv("COGNITO_CLIENT_ID")
	if cognitoClientID == "" {
		log.Fatal("COGNITO_CLIENT_ID environment variable required")
	}

	cognitoClientSecret := os.Getenv("COGNITO_CLIENT_SECRET")
	if cognitoClientSecret == "" {
		log.Fatal("COGNITO_CLIENT_SECRET environment variable required")
	}

	region := "us-west-2"
	cfg := aws.Config{Region: aws.String(region)}
	sess := session.Must(session.NewSession())
	client := dynamodb.New(sess, &cfg)
	cognitoClient := cognitoidentityprovider.New(sess, &cfg)
	userCache := cache.New(30*time.Minute, 1*time.Hour)
	db := services.NewDynamoDB(client, table)

	authClient := auth.CognitoAuthClient{
		Domain:        domain,
		Region:        region,
		ClientID:      cognitoClientID,
		ClientSecret:  cognitoClientSecret,
		PoolID:        cognitoPoolID,
		RedirectURI:   "Prod/callback",
		CognitoClient: cognitoClient,
		UserCache:     userCache,
	}
	if err := authClient.Init(); err != nil {
		log.Fatalf("failed to initialize cognito auth: %v", err)
	}

	api.StartLambda(db, &authClient)
}
