package main

import (
	"flag"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/mkhattab/ww-challenge/api"
	"github.com/mkhattab/ww-challenge/auth"
	"github.com/mkhattab/ww-challenge/services"
)

func main() {
	var (
		endpoint = flag.String("dynamoEndpoint", "", "DynamoDB endpoint")
		region   = flag.String("region", "us-west-2", "AWS region")
		table    = flag.String("table", "ConferenceNotesTable", "DynamoDB table")
		addr     = flag.String("addr", ":8080", "http server bind addr (default: \":8080\")")
		username = flag.String("username", "defaultUser", "username associated with session docs")
		orgID    = flag.String("orgID", "defaultOrgID", "org ID associated with user account")
	)

	flag.Parse()

	cfg := aws.Config{Region: region}
	if *endpoint != "" {
		cfg.Endpoint = endpoint
	}

	sess := session.Must(session.NewSession())
	client := dynamodb.New(sess, &cfg)

	db := services.NewDynamoDB(client, *table)

	api.StartServer(db, *addr, &auth.UserInfo{Username: *username, OrgID: *orgID})
}
