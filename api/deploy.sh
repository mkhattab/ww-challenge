#!/usr/bin/env bash

sam validate -t template.yaml
sam package --region=us-west-2 --template-file template.yaml --s3-bucket conference-notes-api --output-template-file template-packaged.yaml
sam deploy \
    --region=us-west-2 \
    --template-file template-packaged.yaml \
    --stack-name conference-notes-api \
    --capabilities CAPABILITY_IAM
