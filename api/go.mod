module github.com/mkhattab/ww-challenge

go 1.12

require (
	github.com/99designs/gqlgen v0.9.0
	github.com/akrylysov/algnhsa v0.0.0-20190319020909-05b3d192e9a7
	github.com/aws/aws-sdk-go v1.19.36
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/google/uuid v1.1.1
	github.com/lestrrat-go/jwx v0.0.0-20180302000648-0cb38412795e
	github.com/lestrrat-go/pdebug v0.0.0-20180220043849-39f9a71bcabe // indirect
	github.com/mitchellh/mapstructure v0.0.0-20180220230111-00c29f56e238 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/vektah/gqlparser v1.1.2
)
