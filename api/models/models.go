package models

import (
	"encoding/base64"
	"log"
	"time"

	"github.com/google/uuid"
)

// Note represents a note associated with a `Session`
type Note struct {
	ID        string
	Version   int64
	SessionID string `json:"sessionID"`
	Title     string
	Content   string
	Created   time.Time
}

// Session represents a session in a `Conference`
type Session struct {
	ID         string
	Version    int64
	OrgID      string `json:"orgID"`
	Conference Conference
	Title      string
	Created    time.Time
}

func randomID() string {
	randID, err := uuid.Must(uuid.NewRandom()).MarshalBinary()
	if err != nil {
		log.Fatalf("failed to create random UUID: %v", err)
	}
	return base64.RawURLEncoding.EncodeToString(randID)
}

// NewNote returns a new `Note` with an initialized random ID
func NewNote(sessionID, title, content string) *Note {
	id := randomID()

	return &Note{
		ID:        id,
		SessionID: sessionID,
		Title:     title,
		Content:   content,
		Created:   time.Now(),
	}
}

// NewSession returns a new `Session`
func NewSession(orgID string, conference Conference, title string) *Session {
	id := randomID()

	return &Session{
		ID:         id,
		OrgID:      orgID,
		Conference: ConferenceGophercon,
		Title:      title,
		Created:    time.Now(),
	}
}
