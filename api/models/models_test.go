package models

import (
	"testing"
	"time"
)

func TestNote(t *testing.T) {
	t.Run("Create new note", func(t *testing.T) {
		now := time.Now()
		note := NewNote("SessionID", "Foo", "Bar")

		if len(note.ID) != 22 {
			t.Errorf("Invalid unique ID length, expected 22: got %v", note.ID)
		}

		if note.Created.Before(now) || note.Created.Equal(now) {
			t.Errorf("Invalid Created timestamp: got %v", note.Created)
		}

		if note.Title != "Foo" || note.Content != "Bar" {
			t.Errorf("Unexpected values for Title and Content field: got %v", note)
		}
	})
}

func TestSession(t *testing.T) {
	t.Run("Create new session", func(t *testing.T) {
		now := time.Now()
		session := NewSession("OrgID", ConferenceGophercon, "Session Title")

		if len(session.ID) != 22 {
			t.Errorf("Invalid unique ID length, expected 22: got %v", session.ID)
		}

		if session.Created.Before(now) || session.Created.Equal(now) {
			t.Errorf("Invalid Created timestamp: got %v", session.Created)
		}

		if session.Title != "Session Title" {
			t.Errorf("Session title is incorrect: got %v", session.Title)
		}
	})
}
