#!/usr/bin/env bash

set -e

source app.env

function get-resource-id {
    aws cloudformation describe-stack-resources \
                             --stack-name conference-notes-api \
                             --query "StackResources[?ResourceType=='$1'][PhysicalResourceId]" \
                             --output text
}

AWS_REGION="${AWS_REGION:-us-west-2}"
DYNAMODB_TABLE=$(get-resource-id AWS::DynamoDB::Table)
COGNITO_DOMAIN_NAME="${COGNITO_DOMAIN_NAME?COGNITO_DOMAIN_NAME is required (e.g. mytestname)}"
COGNITO_DOMAIN="https://${COGNITO_DOMAIN_NAME}.auth.${AWS_REGION}.amazoncognito.com"

COGNITO_POOL_ID=$(get-resource-id AWS::Cognito::UserPool)
COGNITO_CLIENT_ID=$(get-resource-id AWS::Cognito::UserPoolClient)
COGNITO_CLIENT_SECRET=$(aws cognito-idp describe-user-pool-client \
                            --user-pool-id $COGNITO_POOL_ID \
                            --client-id $COGNITO_CLIENT_ID \
                            --query 'UserPoolClient.ClientSecret' \
                            --output text)
LAMBDA_FUNC=$(get-resource-id AWS::Lambda::Function)
REST_API_ID=$(get-resource-id AWS::ApiGateway::RestApi)
CALLBACK_URL="https://${REST_API_ID}.execute-api.${AWS_REGION}.amazonaws.com/Prod/callback"
SIGNOUT_URL="https://${REST_API_ID}.execute-api.${AWS_REGION}.amazonaws.com/Prod"

if ! aws cognito-idp create-user-pool-domain --user-pool-id $COGNITO_POOL_ID --domain $COGNITO_DOMAIN_NAME 2>/dev/null; then
    echo 'Domain already exists'
fi

aws cognito-idp update-user-pool-client \
    --user-pool-id $COGNITO_POOL_ID \
    --client-id $COGNITO_CLIENT_ID \
    --callback-urls "[\"${CALLBACK_URL}\"]" \
    --logout-urls "[\"${SIGNOUT_URL}\"]" \
    --supported-identity-providers COGNITO \
    --allowed-o-auth-flows-user-pool-client \
    --allowed-o-auth-flows code \
    --allowed-o-auth-scopes email openid aws.cognito.signin.user.admin &>/dev/null

DYNAMODB_TABLE="${DYNAMODB_TABLE?DYNAMO_TABLE is required}"
COGNITO_DOMAIN="${COGNITO_DOMAIN?COGNITO_DOMAIN is required}"
COGNITO_POOL_ID="${COGNITO_POOL_ID?COGNITO_POOL_ID is required}"
COGNITO_CLIENT_ID="${COGNITO_CLIENT_ID?COGNITO_CLIENT_ID is required}"
COGNITO_CLIENT_SECRET="${COGNITO_CLIENT_SECRET?COGNITO_CLIENT_SECRET is required}"

aws apigateway update-rest-api \
    --rest-api-id $REST_API_ID \
    --patch-operations op=replace,path=/endpointConfiguration/types/EDGE,value=REGIONAL &>/dev/null

aws lambda update-function-configuration \
    --function-name $LAMBDA_FUNC \
    --environment \
    Variables="{DYNAMODB_TABLE=${DYNAMODB_TABLE},COGNITO_DOMAIN=${COGNITO_DOMAIN},COGNITO_POOL_ID=${COGNITO_POOL_ID},COGNITO_CLIENT_ID=${COGNITO_CLIENT_ID},COGNITO_CLIENT_SECRET=${COGNITO_CLIENT_SECRET}}" &>/dev/null

aws lambda publish-version --function-name $LAMBDA_FUNC &>/dev/null
