package services

import (
	"context"

	"github.com/mkhattab/ww-challenge/models"
)

// DatabaseService interface
type DatabaseService interface {
	GetSession(ctx context.Context, id, orgID string) (*models.Session, error)
	GetSessions(ctx context.Context, orgID string) ([]*models.Session, error)
	GetSessionVersions(ctx context.Context, id, orgID string) ([]*models.Session, error)
	SaveSession(ctx context.Context, session *models.Session) error

	GetNote(ctx context.Context, id, sessionID string) (*models.Note, error)
	GetNotes(ctx context.Context, sessionID string) ([]*models.Note, error)
	GetNoteVersions(ctx context.Context, id, sessionID string) ([]*models.Note, error)
	SaveNote(ctx context.Context, note *models.Note) error

	WithOption(key string, value interface{})
}
