package services

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/mkhattab/ww-challenge/models"
)

type dynamoService struct {
	client    dynamodbiface.DynamoDBAPI
	tableName string
	options   map[string]interface{}
}

type item struct {
	ID         string
	Version    string
	Title      string
	Content    string
	Conference models.Conference
}

// MaxInt64 used to get max time range
const MaxInt64 = 1<<63 - 1

func rangeParameters(id string, versionFrom, versionTo int64) (string, string) {
	from := fmt.Sprintf("v%d_%d_%s", versionFrom, 0, id)
	to := fmt.Sprintf("v%d_%d_%s", versionTo, MaxInt64, id)
	return from, to
}

func parseRangeKey(key string) (int64, time.Time, string, error) {
	parts := strings.Split(key, "_")
	if len(parts) != 3 {
		return -1, time.Time{}, "", errors.New("failed to parse range key")
	}

	vs := strings.TrimLeft(parts[0], "v")
	version, err := strconv.ParseInt(vs, 10, 64)
	if err != nil {
		return -1, time.Time{}, "", err
	}

	ts, err := strconv.ParseInt(parts[1], 10, 64)
	if err != nil {
		return -1, time.Time{}, "", err
	}
	timestamp := time.Unix(ts, 0)

	return version, timestamp, parts[2], nil
}

func getRangeKey(id string, version int64, ts time.Time) string {
	return fmt.Sprintf("v%d_%d_%s", version, ts.Unix(), id)
}

// NewDynamoDB returns DynamoDB database service
func NewDynamoDB(client dynamodbiface.DynamoDBAPI, tableName string) DatabaseService {
	opts := make(map[string]interface{})
	return &dynamoService{client, tableName, opts}
}

func (s *dynamoService) WithOption(key string, value interface{}) {
	s.options[key] = value
}

func (s *dynamoService) useConsistentReads() bool {
	if x, ok := s.options["useConsistentReads"]; ok {
		v := x.(bool)
		return v
	}
	return false
}

func (s *dynamoService) GetSession(ctx context.Context, id, orgID string) (*models.Session, error) {
	sess := &models.Session{}
	from, to := rangeParameters(id, 0, 0)
	result, err := s.client.QueryWithContext(ctx, &dynamodb.QueryInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":oid":  &dynamodb.AttributeValue{S: aws.String(orgID)},
			":from": &dynamodb.AttributeValue{S: aws.String(from)},
			":to":   &dynamodb.AttributeValue{S: aws.String(to)},
		},
		KeyConditionExpression: aws.String("ID = :oid and Version between :from and :to"),
		Limit:                  aws.Int64(1),
		TableName:              aws.String(s.tableName),
		ConsistentRead:         aws.Bool(s.useConsistentReads()),
	})
	if err != nil {
		return sess, err
	}

	if *result.Count == 0 {
		return sess, errors.New("cannot find session")
	}

	item := item{}
	err = dynamodbattribute.UnmarshalMap(result.Items[0], &item)
	if err != nil {
		return sess, err
	}

	if item.ID != orgID {
		return sess, fmt.Errorf("org ID mismatch: got %v, expected %v", item.ID, orgID)
	}

	version, ts, id, err := parseRangeKey(item.Version)
	if err != nil {
		return sess, err
	}

	sess.ID = id
	sess.Version = version
	sess.OrgID = orgID
	sess.Title = item.Title
	sess.Conference = item.Conference
	sess.Created = ts

	return sess, nil
}

func (s *dynamoService) GetSessions(ctx context.Context, orgID string) ([]*models.Session, error) {
	result, err := s.client.QueryWithContext(ctx, &dynamodb.QueryInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":oid":    &dynamodb.AttributeValue{S: aws.String(orgID)},
			":prefix": &dynamodb.AttributeValue{S: aws.String("v0")},
		},
		KeyConditionExpression: aws.String("ID = :oid and begins_with(Version, :prefix)"),
		TableName:              aws.String(s.tableName),
		ConsistentRead:         aws.Bool(s.useConsistentReads()),
	})
	if err != nil {
		return []*models.Session{}, err
	}

	if *result.Count > 0 {
		items := []item{}
		err := dynamodbattribute.UnmarshalListOfMaps(result.Items, &items)
		if err != nil {
			return []*models.Session{}, err
		}

		sessions := make([]*models.Session, len(items))
		for i, item := range items {
			version, ts, id, err := parseRangeKey(item.Version)
			if err != nil {
				return []*models.Session{}, nil
			}
			sessions[i] = &models.Session{
				ID:         id,
				Version:    version,
				OrgID:      orgID,
				Title:      item.Title,
				Conference: item.Conference,
				Created:    ts,
			}
		}

		return sessions, nil
	}

	return []*models.Session{}, nil

}

func (s *dynamoService) GetSessionVersions(ctx context.Context, id, orgID string) ([]*models.Session, error) {
	from, to := rangeParameters(id, 1, MaxInt64)
	result, err := s.client.QueryWithContext(ctx, &dynamodb.QueryInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":oid":  &dynamodb.AttributeValue{S: aws.String(orgID)},
			":from": &dynamodb.AttributeValue{S: aws.String(from)},
			":to":   &dynamodb.AttributeValue{S: aws.String(to)},
		},
		KeyConditionExpression: aws.String("ID = :oid and Version between :from and :to"),
		TableName:              aws.String(s.tableName),
		ConsistentRead:         aws.Bool(s.useConsistentReads()),
	})
	if err != nil {
		return []*models.Session{}, err
	}

	if *result.Count > 0 {
		items := []item{}
		err := dynamodbattribute.UnmarshalListOfMaps(result.Items, &items)
		if err != nil {
			return []*models.Session{}, err
		}

		sessions := make([]*models.Session, len(items))
		for i, item := range items {
			version, ts, id, err := parseRangeKey(item.Version)
			if err != nil {
				return []*models.Session{}, nil
			}
			sessions[i] = &models.Session{
				ID:         id,
				Version:    version,
				OrgID:      orgID,
				Title:      item.Title,
				Conference: item.Conference,
				Created:    ts,
			}
		}

		return sessions, nil
	}

	return []*models.Session{}, nil

}

func (s *dynamoService) SaveSession(ctx context.Context, session *models.Session) error {
	s.WithOption("useConsistentReads", true)
	defer s.WithOption("useConsistentReads", false)

	versions, err := s.GetSessionVersions(ctx, session.ID, session.OrgID)
	if err != nil {
		return err
	}
	nextVersion := int64(len(versions) + 1)

	itemCurrent, err := dynamodbattribute.MarshalMap(item{
		ID:         session.OrgID,
		Version:    getRangeKey(session.ID, 0, session.Created),
		Title:      session.Title,
		Conference: session.Conference,
	})
	if err != nil {
		return err
	}

	itemNext, err := dynamodbattribute.MarshalMap(item{
		ID:         session.OrgID,
		Version:    getRangeKey(session.ID, nextVersion, session.Created),
		Title:      session.Title,
		Conference: session.Conference,
	})
	if err != nil {
		return err
	}

	_, err = s.client.BatchWriteItemWithContext(ctx, &dynamodb.BatchWriteItemInput{
		RequestItems: map[string][]*dynamodb.WriteRequest{
			s.tableName: {
				{PutRequest: &dynamodb.PutRequest{Item: itemCurrent}},
				{PutRequest: &dynamodb.PutRequest{Item: itemNext}},
			},
		},
	})
	if err != nil {
		return err
	}
	return nil
}

func (s *dynamoService) GetNote(ctx context.Context, id, sessionID string) (*models.Note, error) {
	note := &models.Note{}
	from, to := rangeParameters(id, 0, 0)
	result, err := s.client.QueryWithContext(ctx, &dynamodb.QueryInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":oid":  &dynamodb.AttributeValue{S: aws.String(sessionID)},
			":from": &dynamodb.AttributeValue{S: aws.String(from)},
			":to":   &dynamodb.AttributeValue{S: aws.String(to)},
		},
		KeyConditionExpression: aws.String("ID = :oid and Version between :from and :to"),
		Limit:                  aws.Int64(1),
		TableName:              aws.String(s.tableName),
		ConsistentRead:         aws.Bool(s.useConsistentReads()),
	})
	if err != nil {
		return note, err
	}

	if *result.Count == 0 {
		return note, errors.New("cannot find note")
	}

	item := item{}
	err = dynamodbattribute.UnmarshalMap(result.Items[0], &item)
	if err != nil {
		return note, err
	}

	if item.ID != sessionID {
		return note, fmt.Errorf("session ID mismatch: got %v, expected %v", item.ID, sessionID)
	}

	version, ts, id, err := parseRangeKey(item.Version)
	if err != nil {
		return note, err
	}

	note.ID = id
	note.Version = version
	note.SessionID = sessionID
	note.Title = item.Title
	note.Content = item.Content
	note.Created = ts

	return note, nil
}

func (s *dynamoService) GetNoteVersions(ctx context.Context, id, sessionID string) ([]*models.Note, error) {
	from, to := rangeParameters(id, 1, MaxInt64)
	result, err := s.client.QueryWithContext(ctx, &dynamodb.QueryInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":oid":  &dynamodb.AttributeValue{S: aws.String(sessionID)},
			":from": &dynamodb.AttributeValue{S: aws.String(from)},
			":to":   &dynamodb.AttributeValue{S: aws.String(to)},
		},
		KeyConditionExpression: aws.String("ID = :oid and Version between :from and :to"),
		TableName:              aws.String(s.tableName),
		ConsistentRead:         aws.Bool(s.useConsistentReads()),
	})
	if err != nil {
		return []*models.Note{}, err
	}

	if *result.Count > 0 {
		items := []item{}
		err := dynamodbattribute.UnmarshalListOfMaps(result.Items, &items)
		if err != nil {
			return []*models.Note{}, err
		}

		notes := make([]*models.Note, len(items))
		for i, item := range items {
			version, ts, id, err := parseRangeKey(item.Version)
			if err != nil {
				return []*models.Note{}, nil
			}
			notes[i] = &models.Note{
				ID:        id,
				Version:   version,
				SessionID: sessionID,
				Title:     item.Title,
				Content:   item.Content,
				Created:   ts,
			}
		}

		return notes, nil
	}

	return []*models.Note{}, nil
}

func (s *dynamoService) GetNotes(ctx context.Context, sessionID string) ([]*models.Note, error) {
	result, err := s.client.QueryWithContext(ctx, &dynamodb.QueryInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":oid":    &dynamodb.AttributeValue{S: aws.String(sessionID)},
			":prefix": &dynamodb.AttributeValue{S: aws.String("v0")},
		},
		KeyConditionExpression: aws.String("ID = :oid and begins_with(Version, :prefix)"),
		TableName:              aws.String(s.tableName),
		ConsistentRead:         aws.Bool(s.useConsistentReads()),
	})
	if err != nil {
		return []*models.Note{}, err
	}

	if *result.Count > 0 {
		items := []item{}
		err := dynamodbattribute.UnmarshalListOfMaps(result.Items, &items)
		if err != nil {
			return []*models.Note{}, err
		}

		notes := make([]*models.Note, len(items))
		for i, item := range items {
			version, ts, id, err := parseRangeKey(item.Version)
			if err != nil {
				return []*models.Note{}, nil
			}
			notes[i] = &models.Note{
				ID:        id,
				Version:   version,
				SessionID: sessionID,
				Title:     item.Title,
				Content:   item.Content,
				Created:   ts,
			}
		}

		return notes, nil
	}

	return []*models.Note{}, nil
}

func (s *dynamoService) SaveNote(ctx context.Context, note *models.Note) error {
	s.WithOption("useConsistentReads", true)
	defer s.WithOption("useConsistentReads", false)

	versions, err := s.GetSessionVersions(ctx, note.ID, note.SessionID)
	if err != nil {
		return err
	}
	nextVersion := int64(len(versions) + 1)

	itemCurrent, err := dynamodbattribute.MarshalMap(item{
		ID:      note.SessionID,
		Version: getRangeKey(note.ID, 0, note.Created),
		Title:   note.Title,
		Content: note.Content,
	})
	if err != nil {
		return err
	}

	itemNext, err := dynamodbattribute.MarshalMap(item{
		ID:      note.SessionID,
		Version: getRangeKey(note.ID, nextVersion, note.Created),
		Title:   note.Title,
		Content: note.Content,
	})
	if err != nil {
		return err
	}

	_, err = s.client.BatchWriteItemWithContext(ctx, &dynamodb.BatchWriteItemInput{
		RequestItems: map[string][]*dynamodb.WriteRequest{
			s.tableName: {
				{PutRequest: &dynamodb.PutRequest{Item: itemCurrent}},
				{PutRequest: &dynamodb.PutRequest{Item: itemNext}},
			},
		},
	})
	if err != nil {
		return err
	}
	return nil
}
