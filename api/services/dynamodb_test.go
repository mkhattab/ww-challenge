package services

import (
	"context"
	"os"
	"testing"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/mkhattab/ww-challenge/models"
)

const tableName string = "ConferenceNotesTestDynamoTable"

func setup(t *testing.T, client dynamodbiface.DynamoDBAPI) {
	tableName := aws.String(tableName)
	_, err := client.CreateTable(&dynamodb.CreateTableInput{
		TableName: tableName,
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("ID"),
				KeyType:       aws.String("HASH"),
			},
			{
				AttributeName: aws.String("Version"),
				KeyType:       aws.String("RANGE"),
			},
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("ID"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("Version"),
				AttributeType: aws.String("S"),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(5),
			WriteCapacityUnits: aws.Int64(5),
		},
	})
	if err != nil {
		t.Fatalf("Failed to create test table: got %v", err)
	}

	_, err = client.BatchWriteItem(&dynamodb.BatchWriteItemInput{
		RequestItems: map[string][]*dynamodb.WriteRequest{
			*tableName: {
				// Sessions
				{
					PutRequest: &dynamodb.PutRequest{
						Item: map[string]*dynamodb.AttributeValue{
							"ID":         {S: aws.String("uniqOrgID123")},
							"Version":    {S: aws.String("v0_1000000000_uniqSessID123")},
							"Title":      {S: aws.String("Session title")},
							"Conference": {S: aws.String("Gophercon 2019")},
						},
					},
				},
				{
					PutRequest: &dynamodb.PutRequest{
						Item: map[string]*dynamodb.AttributeValue{
							"ID":         {S: aws.String("uniqOrgID123")},
							"Version":    {S: aws.String("v1_1000000000_uniqSessID123")},
							"Title":      {S: aws.String("Session title")},
							"Conference": {S: aws.String("Gophercon 2019")},
						},
					},
				},
				{
					PutRequest: &dynamodb.PutRequest{
						Item: map[string]*dynamodb.AttributeValue{
							"ID":         {S: aws.String("uniqOrgID123")},
							"Version":    {S: aws.String("v0_1100000000_uniqSessID456")},
							"Title":      {S: aws.String("Session title 2")},
							"Conference": {S: aws.String("Pycon 2019")},
						},
					},
				},
				{
					PutRequest: &dynamodb.PutRequest{
						Item: map[string]*dynamodb.AttributeValue{
							"ID":         {S: aws.String("uniqOrgID456")},
							"Version":    {S: aws.String("v0_1100000000_uniqSessID789")},
							"Title":      {S: aws.String("Session title 2 (other org)")},
							"Conference": {S: aws.String("Gophercon 2019")},
						},
					},
				},

				// Notes
				{
					PutRequest: &dynamodb.PutRequest{
						Item: map[string]*dynamodb.AttributeValue{
							"ID":      {S: aws.String("uniqSessID123")},
							"Version": {S: aws.String("v0_1000000000_uniqNoteID123")},
							"Title":   {S: aws.String("Topic 1")},
							"Content": {S: aws.String("Content 1")},
						},
					},
				},
				{
					PutRequest: &dynamodb.PutRequest{
						Item: map[string]*dynamodb.AttributeValue{
							"ID":      {S: aws.String("uniqSessID123")},
							"Version": {S: aws.String("v1_1000000000_uniqNoteID123")},
							"Title":   {S: aws.String("Topic 1 on Session title")},
							"Content": {S: aws.String("Content 1")},
						},
					},
				},
				{
					PutRequest: &dynamodb.PutRequest{
						Item: map[string]*dynamodb.AttributeValue{
							"ID":      {S: aws.String("uniqSessID123")},
							"Version": {S: aws.String("v0_1100000000_uniqNoteID456")},
							"Title":   {S: aws.String("Topic 2 on Session title 2 ")},
							"Content": {S: aws.String("Content 2")},
						},
					},
				},
				{
					PutRequest: &dynamodb.PutRequest{
						Item: map[string]*dynamodb.AttributeValue{
							"ID":      {S: aws.String("uniqSessID789")},
							"Version": {S: aws.String("v0_1100000000_uniqNoteID789")},
							"Title":   {S: aws.String("Topic 2 on Session title 2 (other org)")},
							"Content": {S: aws.String("Content 2")},
						},
					},
				},
			},
		},
	})
	if err != nil {
		t.Fatalf("Failed to create test items: got %v", err)
	}
}

func teardown(t *testing.T, client dynamodbiface.DynamoDBAPI) {
	_, err := client.DeleteTable(&dynamodb.DeleteTableInput{TableName: aws.String(tableName)})
	if err != nil {
		t.Fatalf("Failed to delete table: got %v", err)
	}
}

func TestRangeParameters(t *testing.T) {
	var tt = []struct {
		msg string

		id          string
		versionFrom int64
		versionTo   int64

		expectedFrom string
		expectedTo   string
	}{
		{"with id", "randomID123", 0, 0, "v0_0_randomID123", "v0_9223372036854775807_randomID123"},
		{"with id", "randomID123", 0, MaxInt64, "v0_0_randomID123", "v9223372036854775807_9223372036854775807_randomID123"},
	}

	for _, ti := range tt {
		t.Run(ti.msg, func(t *testing.T) {
			from, to := rangeParameters(ti.id, ti.versionFrom, ti.versionTo)
			if from != ti.expectedFrom {
				t.Errorf("from got %v, expected %v", from, ti.expectedFrom)
			}

			if to != ti.expectedTo {
				t.Errorf("to got %v, expected %v", to, ti.expectedTo)
			}
		})
	}

}

func TestParseRangeKey(t *testing.T) {
	var tt = []struct {
		msg string

		rangeKey string

		expectedVersion int64
		expectedTime    time.Time
		expectedID      string
	}{
		{"with rangeKey", "v0_1000000000_randomID123", 0, time.Unix(1000000000, 0), "randomID123"},
		{"with max version and time", "v9223372036854775807_9223372036854775807_randomID123", 9223372036854775807, time.Unix(9223372036854775807, 0), "randomID123"},
	}

	for _, ti := range tt {
		t.Run(ti.msg, func(t *testing.T) {
			version, ts, id, err := parseRangeKey(ti.rangeKey)
			if err != nil {
				t.Errorf("err %v", err)
			}

			if version != ti.expectedVersion {
				t.Errorf("version got %v, expected %v", version, ti.expectedVersion)
			}

			if !ts.Equal(ti.expectedTime) {
				t.Errorf("time got %v, expected %v", ts, ti.expectedTime)
			}

			if id != ti.expectedID {
				t.Errorf("id got %v, expected %v", id, ti.expectedID)
			}
		})
	}
}

func TestDynamoDB(t *testing.T) {
	endpoint := os.Getenv("DYNAMODB_ENDPOINT")
	if endpoint == "" {
		t.Fatal("DYNAMODB_ENDPOINT not set")
	}

	cfg := aws.Config{
		Region:   aws.String("us-west-2"),
		Endpoint: aws.String(endpoint),
	}
	sess := session.Must(session.NewSession())
	client := dynamodb.New(sess, &cfg)

	db := NewDynamoDB(client, tableName)
	db.WithOption("useConsistentReads", true)

	t.Run("Get session", func(t *testing.T) {
		setup(t, client)
		defer teardown(t, client)

		ctx := context.Background()
		sess, err := db.GetSession(ctx, "uniqSessID123", "uniqOrgID123")
		if err != nil {
			t.Errorf("Failed to get session: got %v", err)
		}

		if sess.Title != "Session title" {
			t.Errorf("title expected %s, got %s", "Session title", sess.Title)
		}

		if sess.Version != 0 {
			t.Errorf("version got %v, expected 0", sess.Version)
		}

		ts := time.Unix(1000000000, 0)
		if !sess.Created.Equal(ts) {
			t.Errorf("created got %v, expected %v", sess.Created, ts)
		}
	})

	t.Run("Get sessions", func(t *testing.T) {
		setup(t, client)
		defer teardown(t, client)

		ctx := context.Background()
		sessions, err := db.GetSessions(ctx, "uniqOrgID123")
		if err != nil {
			t.Errorf("Failed to get sessions: got %v", err)
		}

		if len(sessions) != 2 {
			t.Errorf("count: got %v, expected 2", len(sessions))
		} else {
			s1 := sessions[0]
			s2 := sessions[1]

			if s1.ID != "uniqSessID123" {
				t.Errorf("first ID: got %v, expected uniqSessID123", s1.ID)
			}

			if s2.ID != "uniqSessID456" {
				t.Errorf("second ID: got %v, expected uniqSessID456", s2.ID)
			}
		}
	})

	t.Run("Get session versions", func(t *testing.T) {
		setup(t, client)
		defer teardown(t, client)

		ctx := context.Background()
		versions, err := db.GetSessionVersions(ctx, "uniqSessID123", "uniqOrgID123")
		if err != nil {
			t.Errorf("Failed to get sessions: got %v", err)
		}

		if len(versions) != 1 {
			t.Errorf("version count got %v, expected 1", len(versions))
		} else {
			sess := versions[0]
			if sess.Version != 1 {
				t.Errorf("Version got %v, expected 1", sess.Version)
			}
		}
	})

	t.Run("Save session", func(t *testing.T) {
		setup(t, client)
		defer teardown(t, client)

		ctx := context.Background()
		orgID := "uniqOrgID890"
		sess := models.NewSession(orgID, "Racketcon", "Lisp session")

		err := db.SaveSession(ctx, sess)
		if err != nil {
			t.Errorf("Failed to save session: got %v", err)
		}

		sess2, err := db.GetSession(ctx, sess.ID, orgID)
		if err != nil {
			t.Errorf("Failed to get session: got %v", err)
		}

		if sess2.ID != sess.ID {
			t.Errorf("ID got %v, expected %v", sess2.ID, sess.ID)
		}

		if sess2.Version != sess.Version {
			t.Errorf("Version got %v, expected %v", sess2.Version, sess.Version)
		}
	})

	t.Run("Save session multiple versions", func(t *testing.T) {
		setup(t, client)
		defer teardown(t, client)

		ctx := context.Background()
		orgID := "uniqOrgID123"
		sessID := "uniqSessID123"
		sess, err := db.GetSession(ctx, sessID, orgID)
		if err != nil {
			t.Errorf("Failed to get session: got %v", err)
		}

		sess.Title = "Updated title"
		err = db.SaveSession(ctx, sess)
		if err != nil {
			t.Errorf("Failed to save session: got %v", err)
		}

		sess2, err := db.GetSession(ctx, sessID, orgID)
		if err != nil {
			t.Errorf("Failed to get updated session: got %v", err)
		}

		if sess2.Title != sess.Title {
			t.Errorf("Title got %v, expected %v", sess2.Title, sess.Title)
		}
	})

	t.Run("Get note", func(t *testing.T) {
		setup(t, client)
		defer teardown(t, client)

		ctx := context.Background()
		sessID := "uniqSessID123"
		note, err := db.GetNote(ctx, "uniqNoteID123", sessID)
		if err != nil {
			t.Errorf("Failed to get note: got %v", err)
		}

		if note.SessionID != sessID {
			t.Errorf("SessionID got %s, expected %s", note.SessionID, sessID)
		}

		if note.Title != "Topic 1" {
			t.Errorf("Title got %s, expected %s", note.Title, "Topic 1")
		}

		if note.Content != "Content 1" {
			t.Errorf("Content got %s, expected %s", note.Title, "Content 1")
		}

		if note.Version != 0 {
			t.Errorf("Version got %v, expected 0", note.Version)
		}

		ts := time.Unix(1000000000, 0)
		if !note.Created.Equal(ts) {
			t.Errorf("Created got %v, expected %v", note.Created, ts)
		}
	})

	t.Run("Get notes", func(t *testing.T) {
		setup(t, client)
		defer teardown(t, client)

		ctx := context.Background()
		notes, err := db.GetNotes(ctx, "uniqSessID123")
		if err != nil {
			t.Errorf("Failed to get notes: got %v", err)
		}

		if len(notes) != 2 {
			t.Errorf("count: got %v, expected 2", len(notes))
		} else {
			s1 := notes[0]
			s2 := notes[1]

			if s1.ID != "uniqNoteID123" {
				t.Errorf("first ID: got %v, expected uniqSessID123", s1.ID)
			}

			if s2.ID != "uniqNoteID456" {
				t.Errorf("second ID: got %v, expected uniqSessID456", s2.ID)
			}
		}
	})

	t.Run("Get note versions", func(t *testing.T) {
		setup(t, client)
		defer teardown(t, client)

		ctx := context.Background()
		versions, err := db.GetNoteVersions(ctx, "uniqNoteID123", "uniqSessID123")
		if err != nil {
			t.Errorf("Failed to get note versions: got %v", err)
		}

		if len(versions) != 1 {
			t.Errorf("version count got %v, expected 1", len(versions))
		} else {
			sess := versions[0]
			if sess.Version != 1 {
				t.Errorf("Version got %v, expected 1", sess.Version)
			}
		}
	})

	t.Run("Save note", func(t *testing.T) {
		setup(t, client)
		defer teardown(t, client)

		ctx := context.Background()
		sessID := "uniqSessID890"
		note := models.NewNote(sessID, "Awesome topic", "Awesome content")

		err := db.SaveNote(ctx, note)
		if err != nil {
			t.Errorf("Failed to save note: got %v", err)
		}

		note2, err := db.GetNote(ctx, note.ID, sessID)
		if err != nil {
			t.Errorf("Failed to get note: got %v", err)
		}

		if note2.ID != note.ID {
			t.Errorf("ID got %v, expected %v", note2.ID, note.ID)

		}

		if note2.Title != note.Title {
			t.Errorf("Title got %v, expected %v", note2.Title, note.Title)
		}

		if note2.Content != note.Content {
			t.Errorf("Content got %v, expected %v", note2.Content, note.Content)
		}

		if note2.Version != note.Version {
			t.Errorf("Version got %v, expected %v", note2.Version, note.Version)
		}
	})

	t.Run("Save note multiple versions", func(t *testing.T) {
		setup(t, client)
		defer teardown(t, client)

		ctx := context.Background()
		sessID := "uniqSessID123"
		noteID := "uniqNoteID123"
		note, err := db.GetNote(ctx, noteID, sessID)
		if err != nil {
			t.Errorf("Failed to get note: got %v", err)
		}

		note.Title = "Updated title"
		err = db.SaveNote(ctx, note)
		if err != nil {
			t.Errorf("Failed to update note: got %v", err)
		}

		note2, err := db.GetNote(ctx, noteID, sessID)
		if err != nil {
			t.Errorf("Failed to get updated note: got %v", err)
		}

		if note2.Title != note.Title {
			t.Errorf("Title got %v, expected %v", note2.Title, note.Title)
		}
	})
}
