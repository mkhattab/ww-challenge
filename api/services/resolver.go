package services

import (
	"context"
	"errors"
	"fmt"
	"log"

	"github.com/mkhattab/ww-challenge/auth"
	"github.com/mkhattab/ww-challenge/generated"
	"github.com/mkhattab/ww-challenge/models"
)

type Resolver struct {
	db DatabaseService
}

func NewResolver(db DatabaseService) generated.Config {
	return generated.Config{
		Resolvers: &Resolver{db},
	}
}

func orgIDFromContext(ctx context.Context) (string, error) {
	val := ctx.Value(auth.UserInfo{})
	if val == nil {
		log.Println("Cannot find userInfo context value:")
		return "", errors.New("missing required userInfo")
	}
	userInfo := val.(*auth.UserInfo)
	return userInfo.OrgID, nil
}

func (r *Resolver) Mutation() generated.MutationResolver {
	return &mutationResolver{r}
}

func (r *Resolver) Query() generated.QueryResolver {
	return &queryResolver{r}
}

func (r *Resolver) Session() generated.SessionResolver {
	return &sessionResolver{r}
}

type mutationResolver struct{ *Resolver }

func (r *mutationResolver) SaveSession(ctx context.Context, input models.SessionInput) (*models.Session, error) {
	orgID, err := orgIDFromContext(ctx)
	if err != nil {
		return nil, err
	}

	var sess *models.Session
	if input.ID == nil || *input.ID == "" {
		sess = models.NewSession(orgID, input.Conference, input.Title)
	} else {
		sess, err = r.db.GetSession(ctx, *input.ID, orgID)
		if err != nil {
			return nil, err
		}

		sess.Title = input.Title
		sess.Conference = input.Conference
	}

	if err = r.db.SaveSession(ctx, sess); err != nil {
		return nil, err
	}

	return sess, nil
}

func (r *mutationResolver) SaveNote(ctx context.Context, input models.NoteInput) (*models.Note, error) {
	orgID, err := orgIDFromContext(ctx)
	if err != nil {
		return nil, err
	}

	sess, err := r.db.GetSession(ctx, input.SessionID, orgID)
	if err != nil {
		return nil, fmt.Errorf("Cannot find parent session: %v", err)
	}

	var note *models.Note
	if input.ID == nil || *input.ID == "" {
		note = models.NewNote(sess.ID, input.Title, input.Content)
	} else {
		note, err = r.db.GetNote(ctx, *input.ID, sess.ID)
		if err != nil {
			return nil, err
		}

		note.Title = input.Title
		note.Content = input.Content
	}

	if err = r.db.SaveNote(ctx, note); err != nil {
		return nil, err
	}

	return note, nil
}

type queryResolver struct{ *Resolver }

func (r *queryResolver) Sessions(ctx context.Context) ([]*models.Session, error) {
	orgID, err := orgIDFromContext(ctx)
	if err != nil {
		log.Println("Error fetching sessions:", err)
		return nil, err
	}

	return r.db.GetSessions(ctx, orgID)
}

func (r *queryResolver) Session(ctx context.Context, id string) (*models.Session, error) {
	orgID, err := orgIDFromContext(ctx)
	if err != nil {
		return nil, err
	}

	return r.db.GetSession(ctx, id, orgID)
}

func (r *queryResolver) SessionVersions(ctx context.Context, id string) ([]*models.Session, error) {
	orgID, err := orgIDFromContext(ctx)
	if err != nil {
		return nil, err
	}

	return r.db.GetSessionVersions(ctx, id, orgID)
}

func (r *queryResolver) Notes(ctx context.Context, sessionID string) ([]*models.Note, error) {
	return r.db.GetNotes(ctx, sessionID)
}

func (r *queryResolver) Note(ctx context.Context, id, sessionID string) (*models.Note, error) {
	return r.db.GetNote(ctx, id, sessionID)
}

func (r *queryResolver) NoteVersions(ctx context.Context, id, sessionID string) ([]*models.Note, error) {
	orgID, err := orgIDFromContext(ctx)
	if err != nil {
		return nil, err
	}

	_, err = r.db.GetSession(ctx, id, orgID)
	if err != nil {
		return nil, err
	}

	return r.db.GetNoteVersions(ctx, id, sessionID)
}

type sessionResolver struct{ *Resolver }

func (r *sessionResolver) Conference(ctx context.Context, obj *models.Session) (models.Conference, error) {
	return obj.Conference, nil
}
func (r *sessionResolver) Notes(ctx context.Context, obj *models.Session) ([]*models.Note, error) {
	return r.db.GetNotes(ctx, obj.ID)
}

type sessionVersionResolver struct{ *Resolver }

func (r *sessionVersionResolver) Conference(ctx context.Context, obj *models.Session) (models.Conference, error) {
	return obj.Conference, nil
}
